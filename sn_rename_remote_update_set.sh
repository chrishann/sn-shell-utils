#!/usr/bin/env sh

[ "$#" -lt 1 ] && {
   echo "usage: $0 filename"
   echo ""
   exit 1
}


FILE_DIR=$(dirname "$1")
FILE_BASENAME=$(basename "$1")

# Extract the Update Set Name from the XML payload
UPDATE_SET_NAME=$(xpath "$1" '/unload/sys_remote_update_set/name/text()' | tail -1)

if [[ $UPDATE_SET_NAME = "" ]]; then
	UPDATE_SET_NAME="UNKNOWN"
fi

# Remove invalid characters from file name
SANITIZED_UPDATE_SET_NAME=$(echo $UPDATE_SET_NAME |  sed -e 's/[^A-Za-z0-9._-]/_/g')

# Extract the source Instance from the "Where From" extended attribute
INSTANCE_ID="UNKNOWN"

WHERE_FROM_XATTR=`mdls -raw -name kMDItemWhereFroms "$1" | tr -d "()\n "`
echo "WHERE_FROM_XATTR: $WHERE_FROM_XATTR" >> ~/Library/Logs/Hazel/Hazel.log
if [[ $WHERE_FROM_XATTR =~ ([a-z0-9]+).service-now.com ]]; then
	INSTANCE_ID="${BASH_REMATCH[1]}"
elif [[ $WHERE_FROM_XATTR =~ localhost ]]; then
	INSTANCE_ID="localhost"
elif [[ $WHERE_FROM_XATTR =~ https://([^/]+)/ ]]; then
	INSTANCE_ID="${BASH_REMATCH[1]}"
fi

# Add the Update Set Name to the File Name
NEW_FILENAME="$SANITIZED_UPDATE_SET_NAME - $FILE_BASENAME"

# Add the Intance Identifier to the file name
if [[ $INSTANCE_ID != "" ]]; then
	NEW_FILENAME="[$INSTANCE_ID] $NEW_FILENAME" 
fi

mv "$1" "$FILE_DIR/$NEW_FILENAME"